import config from './webpack.base.config'
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'

const prodconfig = { ...config }

prodconfig.plugins!.push(
  new HtmlWebpackPlugin({
    template: '!!prerender-loader?string!' + path.resolve(__dirname, 'src', 'index.html')
  }),
)

export default prodconfig