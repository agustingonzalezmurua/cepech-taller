const express = require('express')
const bodyParser = require('body-parser')
const expressValidator = require('express-validator')
const app = express()
const path = require('path')
const port = 80
const assetsPath = path.resolve(__dirname, '..', 'dist')

// Configuration
app.use(bodyParser.json())
app.use(expressValidator())
app.use('/', express.static(assetsPath))
console.info('Static assets loaded from:', assetsPath)


// Routes
app.use('/api', require('./routes/suscriptions'))

// Start
app.listen(port, () => {
  console.info(`[${new Date().toLocaleString()}]`, ': server running at port', port)
})
