module.exports = class Suscription {
  constructor({ nombre, telefono, email, rut, __ID }) {
    /** @type {string} */
    this.nombre = nombre
    /** @type {number} */
    this.telefono = telefono
    /** @type {string} */
    this.email = email
    /** @type {number} */
    this.rut = rut
    /** @type {string} */
    this.__ID = __ID
  }
}