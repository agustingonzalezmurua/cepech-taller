const suscriptionController = require('../controller/suscription')
const express = require('express')
const router = express.Router()

router.post('/suscriptions', suscriptionController.validate('createSuscription'), suscriptionController.createSuscription)
router.get('/suscriptions', (req, res) => res.send('It works!'))

/**
 * Registro de rutas de suscripciones
 */
module.exports = router