const { body, validationResult } = require('express-validator/check')
const uuid = require('uuid/v4')
const AWS = require('aws-sdk')
 
const Suscription = require('../models/suscription')

AWS.config.update({ region: 'us-east-2' })

const docClient = new AWS.DynamoDB.DocumentClient()

const TableName = 'Inscripcion'

/** @param {'createSuscription'} method */
exports.validate = (method) => {
  switch (method) {
    case 'createSuscription': {
      return [
        body('nombre').exists(),
        body('telefono').exists().matches(/(\+56)[0-9]{9}/),
        body('email').exists().isEmail(),
        body('rut').exists().matches(/^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$/)
      ]
    }
  }
}

exports.createSuscription = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  const { nombre, telefono, email, rut } = req.body
  const Item = new Suscription({ nombre, telefono, email, rut, __ID: uuid() })
  const params = {
    TableName,
    Item
  }
  // Inserción de registros en Dynamo
  docClient.put(params, (err, data) => {
    if (err) {
      console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2))
      res.status(500).send('Ocurrió un error al intentar ingresar tus datos, inténtalo de nuevo más tarde.')
    } else {
      console.log("Added item:", JSON.stringify(data, null, 2))
      res.sendStatus(200)
    }
  })
}
