import React from 'react'
import { createGlobalStyle } from 'styled-components'
import { Container, Row } from 'reactstrap'

import NavBar from 'views/navbar'
import MainView from 'views/main'
import RegisterForm from 'views/registerForm'
import Col from 'reactstrap/lib/Col';
import Success from 'views/success';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    height: 100vh;
  }

  #app {
    height: 100%;
  }
`

interface State {
  registered: boolean
}

class App extends React.PureComponent<{}, State> {

  state: State = {
    registered: false
  }

  handleRegistration = () => this.setState({ registered: true })

  render() {
    return (
      <>
        <GlobalStyle />
        <NavBar />
        <MainView>
          <Container fluid>
            <Row>
              {!this.state.registered ? (
                <>
                  <Col md={{ size: 10, offset: 1 }}>
                    <h1>Talleres de Verano</h1>
                    <span>¡No dejes pasar esta increíble oportunidad, inscríbete aquí!</span>
                  </Col>
                  <Col md={{ size: 10, offset: 1 }}>
                    <RegisterForm onRegistration={this.handleRegistration} />
                  </Col>
                </>
              ) :
                (
                  <Success />
                )
              }
            </Row>
          </Container>
        </MainView>
      </>
    )
  }
}

export default App
