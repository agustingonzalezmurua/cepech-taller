import React from 'react'
import styled from 'styled-components'
// @ts-ignore
import bgimage from 'assets/bg.jpeg'

export default styled.section`
  background-image: url(${bgimage});
  background-size: cover;

  height: 100%;

  h1 {
    color: white;
  }
  span {
    color: white;
  }
`