import React from 'react'
import styled from 'styled-components'


function Success(props: any) {
  return(
    <section className={props.className}>
      Has sido registrado exitosamente
    </section>
  )
}

export default styled(Success)`
  width: 100%;
  background-color: white;
  padding: 15px;
  border-radius: 5px;
  text-align: center;
  margin: 15px;
`