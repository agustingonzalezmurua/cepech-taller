import React from 'react'
import styled from 'styled-components'
// @ts-ignore - Typescript no sabe resolver archivos directamente
import logo from 'assets/logo.jpeg'
import { Container, Navbar } from 'reactstrap'
import NavItem from 'reactstrap/lib/NavItem';

const NavContainer = styled.nav`
  background: #F6F6F6;
`

export default () => (
  <NavContainer>
    <Navbar light>
      <Container>
        <a href="http://www.cepech.cl">
          <img src={logo} alt="" />
        </a>
      </Container>
    </Navbar>
  </NavContainer>
)