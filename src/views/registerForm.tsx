import React from 'react'
import styled from 'styled-components'
import { Button, Form, FormGroup, Input, Label, Col } from 'reactstrap'

export interface Props {
  className?: string,
  onRegistration: () => void
}
interface State {
  nombre?: string
  telefono?: string
  email?: string
  rut?: string
}

class RegisterForm extends React.PureComponent<Props, State> {

  state = {
    nombre: '',
    telefono: '',
    email: '',
    rut: ''
  }

  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    const { email, nombre, telefono, rut } = this.state
    const formdata = { email, nombre, telefono, rut }
    event.preventDefault()
    fetch('/api/suscriptions', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formdata)
    }).then(() => {
      this.props.onRegistration()
    }).catch((error) => {
      alert(error.message)
    })
  }

  handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.handleValidation(event)
    this.setState({ [event.target.name]: event.target.value })
  }


  handleValidation = (event: React.FormEvent<HTMLInputElement>) => {
    // No se desconstruye `setCustomValidity` ya que genera un error de invocación ilegal ya que pierde el contexto de ejecución
    const { name, validity } = event.currentTarget
    switch(name) {
      case 'nombre':
        if (validity.valueMissing) {
          event.currentTarget.setCustomValidity('Nombre es obligatorio')
          break
        }
      case 'telefono':
        if (validity.patternMismatch) {
          event.currentTarget.setCustomValidity('Asegúrate de que cumples con el formato +56XXXXXXX')
          break
        }
        if (validity.valueMissing) {
          event.currentTarget.setCustomValidity('Teléfono es obligatorio')
          break
        }
      case 'email':
        if (validity.patternMismatch ) {
          event.currentTarget.setCustomValidity('El correo electrónico debe poseer un formato similar a persona@empresa.pais')
          break
        }
        if (validity.valueMissing ) {
          event.currentTarget.setCustomValidity('Correo es obligatorio')
          break
        }
      case 'rut':
        if (validity.patternMismatch) {
         event.currentTarget.setCustomValidity('Revisa el formato de RUT, debe contener puntos, guión y dígito verificador')
          break
        }
        if (validity.valueMissing) {
         event.currentTarget.setCustomValidity('RUT es obligatorio')
          break
        }
      default:
        event.currentTarget.setCustomValidity('')
        break
    }
  }

  render() {
    return (
      <Form className={this.props.className} id="tallerForm" onSubmitCapture={this.handleSubmit} ref={this.form}>
        <FormGroup>
          <Label for='nombre'>Nombre *</Label>
          <Input name='nombre' defaultValue='' placeholder='José Canseco' type='text' required maxLength={250} onChange={this.handleInputChange} onInvalid={this.handleValidation}/>
        </FormGroup>
        <FormGroup>
          <Label for='telefono'>Teléfono *</Label>
          <Input name='telefono' defaultValue='' placeholder='+56942660052' pattern='(\+56)[0-9]{9}' type='tel' required maxLength={12} onChange={this.handleInputChange} onInvalid={this.handleValidation}/>
        </FormGroup>
        <FormGroup>
          <Label for='email'>Correo *</Label>
          <Input name='email' defaultValue='' placeholder='correo@gmail.com' pattern='[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*' required onChange={this.handleInputChange} onInvalid={this.handleValidation}/>
        </FormGroup>
        <FormGroup label='RUT'>
          <Label for='rut'>RUT *</Label>
          <Input name='rut' defaultValue='' placeholder='26.492.283-1' pattern='^(\d{1,3}(?:\.\d{1,3}){2}-[\dkK])$' required onChange={this.handleInputChange} onInvalid={this.handleValidation}/>
        </FormGroup>
        <FormGroup>
          <Button color='primary' size='lg' block>Inscríbete</Button>
        </FormGroup>
      </Form>
    )
  }
}

export default styled(RegisterForm)`
  label {
    color: white;
  }
`
