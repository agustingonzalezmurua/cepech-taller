import React from 'react'
import ReactDOM from 'react-dom'
import { renderToString } from 'react-dom/server'

import 'reset-css'
import 'bootstrap/dist/css/bootstrap.min.css'
import App from '@app'

ReactDOM.render(<App/>, document.getElementById('app'))

// Only for prerender-loader
export default () => {
  renderToString(<App />)
}
