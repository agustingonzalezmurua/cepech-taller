import config from './webpack.base.config'
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'

const devconfig = { ...config }

devconfig.plugins!.push(
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, 'src', 'index.html')
  }),
)

devconfig.devServer = {
  proxy: {
    '/api': 'http://localhost:80'
  }
}

devconfig.devtool = 'source-map'

export default devconfig
